//1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const elements = document.querySelectorAll("p");

for (let element of elements) {
  element.style.backgroundColor = "#ff0000";
};

/*2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
     Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.*/
const item = document.getElementById("optionsList");
console.log(item);

//батьківський елемент
console.log("-------------------------------------------");
console.log("Родительский элемент", item.parentElement);

//дочірні ноди
console.log("-------------------------------------------");
console.log("  Дочерние ноды");
for (let i = 0; i < item.childNodes.length; i++) {
  console.log("  Имя ноды:", item.childNodes[i].nodeName, "  Тип ноды:", item.childNodes[i].nodeType); 
};

//3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - "This is a paragraph"
console.log("-------------------------------------------");
const searchParagraph = (document.getElementById("testParagraph").innerHTML = "This is a paragraph");
console.log("Изменен контент элемента на:", searchParagraph);

/*4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
     Кожному з елементів присвоїти новий клас nav-item.*/
console.log("-------------------------------------------");
const mainHeader = document.querySelector(".main-header");
console.log("Элемент с классом \"main-header\"", mainHeader);

for (let i = 0; i < mainHeader.children.length; i++) {
  console.log("- Дочерний элемент:", mainHeader.children[i]); 
  mainHeader.children[i].classList.add("nav-item");
  console.log("  Добавлен класс \"nav-item\":", mainHeader.children[i]); 
};

// 5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
console.log("-------------------------------------------");
let secTite = document.querySelectorAll(".section-title");

for (let element of secTite) {
  element.classList.remove("section-title");
  console.log("Удален класс \"section-title\" у элемента", element);
};
